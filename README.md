# Search in Vector DB

FastApi service that provide endpoints to add and search wiki articles.
Used [Qdrant](https://qdrant.tech) Vector Database and SentenceTransformer with `all-MiniLM-L6-v2` model

## How to run

- clone this repo
- run `docker compose up` in root dir
- wait until you see text like: `Uvicorn running on http://0.0.0.0:8000 (Press CTRL+C to quit)`
- open `http://0.0.0.0:8000/docs` in browser
- run `http://127.0.0.1:8000/docs#/default/init_api_init_post` to initialize DB with test wiki articles
- use `http://127.0.0.1:8000/docs#/default/search_api_search_post` to test search

> To use `api/search` with `text` parameter please clear `Request body` that is filled with `[0]` by default 