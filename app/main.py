import json
import wikipediaapi

from typing import List
from fastapi import FastAPI, Depends, HTTPException
from pydantic import BaseModel, model_validator
from qdrant_client.models import Filter
from qdrant_client.http.exceptions import UnexpectedResponse
from app.searcher import Searcher
from multiprocessing.pool import ThreadPool


app = FastAPI(
    title="Vector DB search API",
    description="API to adding and searching items in vector database",
)

searcher = Searcher(collection_name="wiki")


class Query(BaseModel):
    text: str | None = None
    embedding: None | list[float] = None
    query_filter: str | None = None
    limit: int = 3
    invert: bool = False

    @model_validator(mode="before")
    def validate(cls, values):
        if (
            len(
                [
                    k
                    for k, v in values.items()
                    if v is not None and k in ["text", "embedding"]
                ]
            )
            != 1
        ):
            raise HTTPException(
                422, "Exactly one of text or embedding must be provided"
            )
        return values


@app.post("/api/search")
async def search(query: Query = Depends()):
    """
    Search items in the vector db.
    Can be performed by `text` or `embedding`.

    Parameters:
    - text (str, optional): text to to search.
    - query_filter (float, optional): filter results. Example: `categories=Animal`
    - limit (int): Get top `K` results.
    - invert (bool): Invert order of results by `score`.

    Request body (List[float], optional): vector embedding.
    """
    f = query.query_filter
    if f:
        key, value = f.split("=")
        f = Filter(**{"must": [{"key": key, "match": {"text": value}}]})
    limit = query.limit
    try:
        invert = query.invert
        query = searcher.to_embedding(query.text) if query.text else query.embedding
        hits = await searcher.search(query, limit, f)
        r = [{"payload": hit.payload, "score": hit.score} for hit in hits]
        return r if not invert else sorted(r, key=lambda x: x["score"])
    except UnexpectedResponse as e:
        e = json.loads(e.content.decode("utf-8"))
        raise HTTPException(status_code=400, detail=e["status"]["error"])


@app.get("/api/embedding")
async def to_embedding(text: str):
    """
    Convert text string to vector embedding.
    """
    return searcher.to_embedding(text)


@app.post("/api/init")
async def init():
    """
    Initialize vector DB with some test wiki articles:
    - REST
    - python
    - Rust
    - Pytest
    - Selenium
    - Selenium_(software)
    - Rust_(programming_language)
    - Python_(programming_language)
    """
    return await searcher.add(
        get_wiki_pages(
            [
                "REST",
                "python",
                "Rust",
                "Pytest",
                "Selenium",
                "Selenium_(software)",
                "Rust_(programming_language)",
                "Python_(programming_language)",
            ]
        )
    )


@app.post("/api/article")
async def add_article(articles: List[str]):
    """
    Add wiki articles to vector DB.

    Example:

    ```python
    ["Go_(programming_language)"] # For next article url https://en.wikipedia.org/wiki/Go_(programming_language)
    ```
    """
    return await searcher.add(get_wiki_pages(articles))


def get_wiki_page(article: str) -> dict:
    wiki_wiki = wikipediaapi.Wikipedia("Test (test@example.com)", "en")
    page = wiki_wiki.page(article)
    return {
        "title": page.title,
        "text": page.text,
        "categories": list(page.categories.keys()),
    }


def get_wiki_pages(articles: List[str]) -> List[dict]:
    pool = ThreadPool(processes=1)
    async_results = [
        pool.apply_async(get_wiki_page, (article,)) for article in articles
    ]
    return [async_result.get() for async_result in async_results]


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
