from typing import List
from qdrant_client import models, QdrantClient, AsyncQdrantClient
from qdrant_client.http.exceptions import UnexpectedResponse
from sentence_transformers import SentenceTransformer


class Searcher:
    def __init__(self, collection_name):
        self.collection_name = collection_name
        self.model = SentenceTransformer("all-MiniLM-L6-v2", device="cpu")
        self.qdrant = AsyncQdrantClient("http://qdrant:6333")
        client = QdrantClient("http://qdrant:6333")

        try:
            client.count(self.collection_name)
        except (ValueError, UnexpectedResponse):
            client.recreate_collection(
                collection_name=self.collection_name,
                vectors_config=models.VectorParams(
                    size=self.model.get_sentence_embedding_dimension(),
                    distance=models.Distance.COSINE,
                ),
            )

    def to_embedding(self, text: str):
        return self.model.encode(text).tolist()

    async def search(self, vector: list, limit: int = 5, filter=None):
        return await self.qdrant.search(
            collection_name=self.collection_name,
            query_vector=vector,
            query_filter=filter,
            limit=limit,
        )

    async def add(self, items: List[str]):
        n = await self.qdrant.count(self.collection_name)
        n = n.count
        await self.qdrant.upsert(
            collection_name=self.collection_name,
            points=[
                models.PointStruct(
                    id=n + idx,
                    vector=self.model.encode(doc["text"]).tolist(),
                    payload=doc,
                )
                for idx, doc in enumerate(items)
            ],
        )
        return n + len(items)
